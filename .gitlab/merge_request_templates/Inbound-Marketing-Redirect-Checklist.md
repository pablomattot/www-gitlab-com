This checklist is based on the [Inbound Marketing](https://about.gitlab.com/handbook/marketing/inbound-marketing/) team's documentation for implementing URL redirects.

- [ ] Relevant pages have been moved.
- [ ] The pages still function as intended after being moved.
- [ ] Relevant canonical URLs have been updated (if any).
- [ ] Redirect rules have been created.
- [ ] The redirect works as intended in production (don't forget to verify after release).
